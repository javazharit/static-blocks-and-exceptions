package me.babayan;

public class StaticExample {
    /**
     * static block
    * */
    static {
        try {
            System.out.println("This is first static block");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public StaticExample() {
        System.out.println("This is constructor");
    }

    /**
     * static variable
     * */
    public static String staticString = "Static Variable";

    /**
     * static block
     * */
    static {
        System.out.println("This is second static block and " + staticString);
    }

    /**
     * static block
     * */
    static {
        staticMethod();
        System.out.println("This is third static block");
    }

    /**
     * static method
     * */
    public static void staticMethod() {
        try {System.out.println("This is static method");
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * static method
     * */
    public static void staticMethod2() {
        System.out.println("This is static method2");
    }

    public static void main(String[] args) {
        StaticExample staticExample = new StaticExample();
        StaticExample.staticMethod2();
    }
}